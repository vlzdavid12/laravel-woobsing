<x-guest-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="font-semibold">{{ __('Autenticar Dos Factores') }}</div>


                <p class="text-center">
                    {{ __('Ingresa a la app y scanea el Qr y ingresa el codigo de ingreso.') }}

                </p>
                <a class="block text-center text-green-600" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=es_CO&gl=US&pli=1" target="_blank">Descarga App</a>
                <form method="POST" action="{{ route('two-factor.login') }}">
                    @csrf

                    <div class="mt-4">
                        <x-input-label for="code" :value="__('Ingresa el codigo de Google Authenticator')"/>
                        <x-text-input id="code" class="block mt-1 w-full" type="text" name="code"
                                      :value="old('code')"/>
                        <x-input-error :messages="$errors->get('code')" class="mt-2"/>
                    </div>

                    <div class="flex justify-center">
                        <x-primary-button class="mt-4 block">
                            {{ __('Ingresar') }}
                        </x-primary-button>
                    </div>

                </form>

                <div class="mt-3 font-semibold">{{ __('Código de Recuperación de Dos Factores') }}</div>


                <p class="text-center">
                    {{ __('Ingresa el codigo que se genero al comenzar al iniciar sesión.') }}
                </p>

                <form method="POST" action="{{ route('two-factor.login') }}">
                    @csrf

                    <div class="mt-4">
                        <x-input-label for="recovery_code" :value="__('Authentication code')"/>
                        <x-text-input id="recovery_code" class="block mt-1 w-full" type="text" name="recovery_code"
                                      :value="old('recovery_code')"/>
                        <x-input-error :messages="$errors->get('recovery_code')" class="mt-2"/>
                    </div>

                    <div class="flex justify-center">
                        <x-primary-button class="mt-4 block">
                            {{ __('Ingresar') }}
                        </x-primary-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-guest-layout>
